package pt.fpapps.flickrapi.detail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TitlePojo {
    @JsonProperty("_content")
    private String content;

    public String getContent() {
        return content;
    }
}
