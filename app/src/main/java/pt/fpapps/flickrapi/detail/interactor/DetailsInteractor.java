package pt.fpapps.flickrapi.detail.interactor;

import io.reactivex.Single;
import pt.fpapps.flickrapi.detail.model.PhotoDetailResponsePojo;
import pt.fpapps.flickrapi.detail.repository.DetailsApiRepository;
import retrofit2.Response;

public class DetailsInteractor {

    private DetailsApiRepository detailsApiRepository;

    public DetailsInteractor(DetailsApiRepository detailsApiRepository) {
        this.detailsApiRepository = detailsApiRepository;
    }

    public Single<Response<PhotoDetailResponsePojo>> getPhotoDetails(String photoId) {
        return detailsApiRepository.getPhotoDetails(photoId);
    }
}
