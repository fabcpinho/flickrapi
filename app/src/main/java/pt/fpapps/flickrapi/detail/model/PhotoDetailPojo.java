package pt.fpapps.flickrapi.detail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoDetailPojo {
    @JsonProperty("id")
    private String id;

    @JsonProperty("title")
    private TitlePojo title;

    @JsonProperty("description")
    private DescriptionPojo description;

    public String getId() {
        return id;
    }

    public TitlePojo getTitle() {
        return title;
    }

    public DescriptionPojo getDescription() {
        return description;
    }
}
