package pt.fpapps.flickrapi.detail.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import pt.fpapps.flickrapi.R;
import pt.fpapps.flickrapi.dagger.component.DaggerDetailsActivityComponent;
import pt.fpapps.flickrapi.dagger.module.DetailsInteractorModule;
import pt.fpapps.flickrapi.detail.interactor.DetailsInteractor;
import pt.fpapps.flickrapi.detail.model.PhotoDetailPojo;
import pt.fpapps.flickrapi.detail.presenter.DetailViewModel;
import pt.fpapps.flickrapi.detail.view.contract.DetailsViewContract;

public class DetailActivity extends AppCompatActivity implements DetailsViewContract {
    public final static String PHOTO_ID_KEY = "photo_id";
    public final static String PHOTO_URL = "photo_url";

    @Inject
    DetailsInteractor detailsInteractor;
    private TextView details;
    private ContentLoadingProgressBar detailsProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        initDagger();

        String photoId = getIntent().getExtras().getString(PHOTO_ID_KEY);
        String photoURL = getIntent().getExtras().getString(PHOTO_URL);

        ImageView iv = findViewById(R.id.image);
        details = findViewById(R.id.details);
        detailsProgressBar = findViewById(R.id.detailsProgressBar);

        Picasso.get().load(photoURL).fit().centerCrop().into(iv);

        DetailViewModel viewModel = ViewModelProviders.of(this).get(DetailViewModel.class);
        viewModel.init(detailsInteractor, this);
        viewModel.getPhotoDetails(photoId);

    }

    private void initDagger() {
        DaggerDetailsActivityComponent.builder()
                .detailsInteractorModule(new DetailsInteractorModule())
                .build()
                .inject(this);
    }

    /**
     * Not worrying too much about creating beautiful views to show the content
     * @param detailsPojo details to show
     */
    @Override
    public void showDetails(PhotoDetailPojo detailsPojo) {
        details.setText(String.format(getString(R.string.details_text), detailsPojo.getTitle().getContent(), detailsPojo.getDescription().getContent()));
        detailsProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        details.setText(getString(R.string.could_not_load_details));
        detailsProgressBar.setVisibility(View.GONE);
    }
}
