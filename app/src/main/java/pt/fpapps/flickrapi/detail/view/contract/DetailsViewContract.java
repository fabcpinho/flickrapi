package pt.fpapps.flickrapi.detail.view.contract;

import pt.fpapps.flickrapi.detail.model.PhotoDetailPojo;

public interface DetailsViewContract {
    void showDetails(PhotoDetailPojo details);

    void showError();
}
