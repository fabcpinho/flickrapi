package pt.fpapps.flickrapi.detail.presenter;

import android.arch.lifecycle.ViewModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import pt.fpapps.flickrapi.detail.interactor.DetailsInteractor;
import pt.fpapps.flickrapi.detail.view.contract.DetailsViewContract;

public class DetailViewModel extends ViewModel {

    private DetailsInteractor detailsInteractor;
    private DetailsViewContract detailsViewContract;
    private CompositeDisposable compositeDisposable;

    public void init(DetailsInteractor detailsInteractor, DetailsViewContract detailsViewContract) {
        this.detailsInteractor = detailsInteractor;
        this.detailsViewContract = detailsViewContract;
        compositeDisposable = new CompositeDisposable();
    }

    public void getPhotoDetails(String photoId) {
        compositeDisposable.add(detailsInteractor.getPhotoDetails(photoId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(photoDetailResponsePojoResponse -> detailsViewContract.showDetails(photoDetailResponsePojoResponse.body().getPhotoDetailPojo()), throwable -> detailsViewContract.showError()));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}
