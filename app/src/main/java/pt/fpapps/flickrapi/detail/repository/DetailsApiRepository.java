package pt.fpapps.flickrapi.detail.repository;

import io.reactivex.Single;
import pt.fpapps.flickrapi.detail.model.PhotoDetailResponsePojo;
import pt.fpapps.flickrapi.photos.api.PublicPhotosApiContract;
import retrofit2.Response;

import static pt.fpapps.flickrapi.api.ApiUtils.API_KEY;

public class DetailsApiRepository {

    private PublicPhotosApiContract publicPhotosApiContract;

    public DetailsApiRepository(PublicPhotosApiContract publicPhotosApiContract) {

        this.publicPhotosApiContract = publicPhotosApiContract;
    }

    public Single<Response<PhotoDetailResponsePojo>> getPhotoDetails(String photoId){
        return publicPhotosApiContract.getPhotoDetails(API_KEY, photoId);
    }
}
