package pt.fpapps.flickrapi.detail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoDetailResponsePojo {
    @JsonProperty("photo")
    private PhotoDetailPojo photoDetailPojo;

    public PhotoDetailPojo getPhotoDetailPojo() {
        return photoDetailPojo;
    }
}
