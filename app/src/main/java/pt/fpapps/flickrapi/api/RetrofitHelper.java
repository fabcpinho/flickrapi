package pt.fpapps.flickrapi.api;

import android.support.annotation.NonNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pt.fpapps.flickrapi.photos.api.PublicPhotosApiContract;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitHelper {

    /**
     * This method configures a retrofit instance of PublicPhotosApiContract.
     *
     * @param retrofit A retrofit instance
     * @return a PublicPhotosApiContract instance
     */
    public PublicPhotosApiContract makeARestPhotosService(Retrofit retrofit) {
        if (retrofit != null) {
            return retrofit.create(PublicPhotosApiContract.class);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Configures the Retrofit builder
     *
     * @param formattedUrl an url already formatted (by getBaseUrl)
     * @return a configured retrofit instance
     */
    public Retrofit configureRetrofitBuilder(@NonNull String formattedUrl) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(formattedUrl);
        builder.client(client);
        builder.addConverterFactory(JacksonConverterFactory.create());
        builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        return builder.build();
    }
}
