package pt.fpapps.flickrapi.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pt.fpapps.flickrapi.api.RetrofitHelper;

@Module
public class RetrofitHelperModule {

  @Singleton
  @Provides
  public RetrofitHelper retrofitHelper(){
    return new RetrofitHelper();
  }
}
