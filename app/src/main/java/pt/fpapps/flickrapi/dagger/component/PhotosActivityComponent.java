package pt.fpapps.flickrapi.dagger.component;

import javax.inject.Singleton;

import dagger.Component;
import pt.fpapps.flickrapi.dagger.module.PhotosInteractorModule;
import pt.fpapps.flickrapi.dagger.module.RxSchedulersModule;
import pt.fpapps.flickrapi.photos.view.PhotosActivity;

@Singleton
@Component(modules = {
        PhotosInteractorModule.class,
        RxSchedulersModule.class
})
public interface PhotosActivityComponent {
    void inject(PhotosActivity photosActivity);
}
