package pt.fpapps.flickrapi.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pt.fpapps.flickrapi.rxjava.RxSchedulers;

@Module
public class RxSchedulersModule {
  @Provides
  @Singleton
  public RxSchedulers providesRxSchedulers() {
    return new RxSchedulers();
  }
}
