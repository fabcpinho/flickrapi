package pt.fpapps.flickrapi.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pt.fpapps.flickrapi.detail.interactor.DetailsInteractor;
import pt.fpapps.flickrapi.detail.repository.DetailsApiRepository;

@Module(includes = DetailsRepositoryModule.class)
public class DetailsInteractorModule {

    @Provides
    @Singleton
    public DetailsInteractor providesDetailsInteractor(DetailsApiRepository detailsApiRepository) {
        return new DetailsInteractor(detailsApiRepository);
    }
}
