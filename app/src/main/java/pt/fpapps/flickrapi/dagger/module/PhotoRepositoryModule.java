package pt.fpapps.flickrapi.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pt.fpapps.flickrapi.photos.api.PublicPhotosApiContract;
import pt.fpapps.flickrapi.photos.repository.PhotosRepository;

@Module(includes = PublicPhotosApiContractModule.class)
public class PhotoRepositoryModule {

    @Provides
    @Singleton
    public PhotosRepository providesPhotosRepository(PublicPhotosApiContract publicPhotosApiContract) {
        return new PhotosRepository(publicPhotosApiContract);
    }
}
