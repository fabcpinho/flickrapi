package pt.fpapps.flickrapi.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pt.fpapps.flickrapi.api.RetrofitHelper;
import pt.fpapps.flickrapi.photos.api.PublicPhotosApiContract;
import retrofit2.Retrofit;

@Module(includes = {
    RetrofitHelperModule.class,
    RetrofitModule.class
})
public class PhotosApiContractModule {
  @Provides
  @Singleton
  PublicPhotosApiContract getPhotosApiContract(RetrofitHelper retrofitHelper, Retrofit retrofit) {
    return retrofitHelper.makeARestPhotosService(retrofit);
  }
}
