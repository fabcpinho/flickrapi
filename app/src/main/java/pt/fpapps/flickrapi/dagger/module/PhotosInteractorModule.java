package pt.fpapps.flickrapi.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pt.fpapps.flickrapi.photos.interactor.PhotosInteractor;
import pt.fpapps.flickrapi.photos.repository.PhotosRepository;

@Module(includes = PhotoRepositoryModule.class)
public class PhotosInteractorModule {

    @Provides
    @Singleton
    public PhotosInteractor providesPhotoInteractor(PhotosRepository photosRepository) {
        return new PhotosInteractor(photosRepository);
    }
}
