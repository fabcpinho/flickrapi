package pt.fpapps.flickrapi.dagger.component;

import javax.inject.Singleton;

import dagger.Component;
import pt.fpapps.flickrapi.dagger.module.DetailsInteractorModule;
import pt.fpapps.flickrapi.detail.view.DetailActivity;

@Singleton
@Component(modules = {
        DetailsInteractorModule.class
})
public interface DetailsActivityComponent {
    void inject(DetailActivity photosActivity);
}
