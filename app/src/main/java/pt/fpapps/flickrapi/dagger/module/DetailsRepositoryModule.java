package pt.fpapps.flickrapi.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pt.fpapps.flickrapi.detail.repository.DetailsApiRepository;
import pt.fpapps.flickrapi.photos.api.PublicPhotosApiContract;

@Module(includes = PublicPhotosApiContractModule.class)
public class DetailsRepositoryModule {

    @Provides
    @Singleton
    public DetailsApiRepository providesDetailsApiRepository(PublicPhotosApiContract publicPhotosApiContract) {
        return new DetailsApiRepository(publicPhotosApiContract);
    }
}
