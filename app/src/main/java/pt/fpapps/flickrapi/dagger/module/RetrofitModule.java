package pt.fpapps.flickrapi.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pt.fpapps.flickrapi.api.RetrofitHelper;
import retrofit2.Retrofit;

@Module(includes = RetrofitHelperModule.class)
public class RetrofitModule {
    private String url = "https://api.flickr.com/services/rest/";

    @Provides
    @Singleton
    public Retrofit providesRetrofit(RetrofitHelper retrofitHelper) {
        return retrofitHelper.configureRetrofitBuilder(url);
    }
}
