package pt.fpapps.flickrapi.photos.adapter;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import pt.fpapps.flickrapi.R;
import pt.fpapps.flickrapi.photos.model.photo.PhotoPojo;
import pt.fpapps.flickrapi.photos.view.contract.PhotosListViewContract;

public class PhotosListAdapter extends PagedListAdapter<PhotoPojo, PhotosListAdapter.SearchListViewHolder> {

    private PhotosListViewContract view;

    public PhotosListAdapter(PhotosListViewContract view) {
        super(DIFF_CALLBACK);
        this.view = view;
    }

    @NonNull
    @Override
    public SearchListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
        return new SearchListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchListViewHolder holder, int position) {
        holder.bindTo(position, getItem(position));
    }

    public void updateItem(int rvPosition, String url) {
        getCurrentList().get(rvPosition).setPhotoUrl(url);
        notifyDataSetChanged();
    }

    class SearchListViewHolder extends RecyclerView.ViewHolder {

        private AppCompatImageView imageView;

        private SearchListViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageItem);
        }

        void bindTo(int position, PhotoPojo photo) {
            if (photo != null && photo.getPhotoUrl() != null) {
                Picasso.get().load(photo.getPhotoUrl()).fit().centerCrop().into(imageView);

                imageView.setOnClickListener(v -> view.goToDetail(photo.getId(), photo.getPhotoUrl()));
            } else if(photo != null){
                view.getImageUrl(position, photo.getId());
            }
        }
    }

    private static final DiffUtil.ItemCallback<PhotoPojo> DIFF_CALLBACK = new DiffUtil.ItemCallback<PhotoPojo>() {

        @Override
        public boolean areItemsTheSame(PhotoPojo oldItem, PhotoPojo newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(PhotoPojo oldItem, PhotoPojo newItem) {
            return true;
        }
    };
}
