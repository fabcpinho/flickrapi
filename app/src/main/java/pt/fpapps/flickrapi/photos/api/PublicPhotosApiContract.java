package pt.fpapps.flickrapi.photos.api;

import io.reactivex.Single;
import pt.fpapps.flickrapi.detail.model.PhotoDetailResponsePojo;
import pt.fpapps.flickrapi.photos.model.photo.PublicPhotosResponsePojo;
import pt.fpapps.flickrapi.photos.model.size.PhotoSizesResponsePojo;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PublicPhotosApiContract {
    @GET("?method=flickr.people.getPublicPhotos&nojsoncallback=true&format=json")
    Single<Response<PublicPhotosResponsePojo>> getPublicPhotos(@Query(value = "user_id", encoded = true) String userId, @Query("api_key") String apiKey, @Query("per_page") int perPage, @Query("page") int page);

    @GET("?method=flickr.photos.getSizes&nojsoncallback=true&format=json")
    Single<Response<PhotoSizesResponsePojo>> getPhotoSizes(@Query("api_key") String apiKey, @Query("photo_id") String photoId);

    @GET("?method=flickr.photos.getInfo&nojsoncallback=true&format=json")
    Single<Response<PhotoDetailResponsePojo>> getPhotoDetails(@Query("api_key") String apiKey, @Query("photo_id") String photoId);
}