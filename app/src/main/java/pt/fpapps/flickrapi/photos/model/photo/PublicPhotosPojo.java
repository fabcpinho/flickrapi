package pt.fpapps.flickrapi.photos.model.photo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PublicPhotosPojo {
    private int page;
    private int pages;
    private int perpage;
    private String total;
    @JsonProperty("photo")
    private List<PhotoPojo> photo;

    public String getTotal() { return total; }

    public int getPage() {
        return page;
    }

    public int getPages() {
        return pages;
    }

    public int getPerPage() {
        return perpage;
    }

    public List<PhotoPojo> getPhotosList() {
        return photo;
    }
}
