package pt.fpapps.flickrapi.photos.interactor;

import java.util.List;

import io.reactivex.Single;
import pt.fpapps.flickrapi.photos.model.photo.PhotoPojo;
import pt.fpapps.flickrapi.photos.model.size.SizePojo;
import pt.fpapps.flickrapi.photos.repository.PhotosRepository;

public class PhotosInteractor {
    private final static String IMAGE_SIZE_LABEL = "Medium 640";

    private PhotosRepository photosRepository;

    public PhotosInteractor(PhotosRepository photosRepository) {
        this.photosRepository = photosRepository;
    }

    public Single<List<PhotoPojo>> getPublicPhotos(int perPage, int page) {
        return photosRepository.getPhotosFromApi(perPage, page).map(photosResponse -> {
            if (photosResponse.body() != null) {
                return photosResponse.body().getPublicPhotosPojo().getPhotosList();
            } else return null;
        });
    }

    public Single<SizePojo> getPhotoSizes(String photoId) {
        return photosRepository.getPhotoSizes(photoId).map(sizeResponse -> {
            if (sizeResponse.body() != null) {
                List<SizePojo> sizes = sizeResponse.body().getPhotoSizesPojo().getSizes();
                /*for (SizePojo size : sizes) {
                    if (size.getLabel().equals(IMAGE_SIZE_LABEL))
                        return size;
                }*/

                //NOTE: returning the element we can improve the photo loading speed, but we're trusting that the API keep sizes in the same order
                return sizes.get(6);

            } else return null;
        });
    }


}
