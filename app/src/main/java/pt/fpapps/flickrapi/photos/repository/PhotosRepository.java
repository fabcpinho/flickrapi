package pt.fpapps.flickrapi.photos.repository;

import io.reactivex.Single;
import pt.fpapps.flickrapi.photos.api.PublicPhotosApiContract;
import pt.fpapps.flickrapi.photos.model.photo.PublicPhotosResponsePojo;
import pt.fpapps.flickrapi.photos.model.size.PhotoSizesResponsePojo;
import retrofit2.Response;

import static pt.fpapps.flickrapi.api.ApiUtils.API_KEY;
import static pt.fpapps.flickrapi.api.ApiUtils.USER_ID;

public class PhotosRepository {
    private PublicPhotosApiContract photosApiContract;

    public PhotosRepository(PublicPhotosApiContract photosApiContract) {
        this.photosApiContract = photosApiContract;
    }

    public Single<Response<PublicPhotosResponsePojo>> getPhotosFromApi(int perPage, int page) {
        return photosApiContract.getPublicPhotos(USER_ID, API_KEY, perPage, page);
    }

    public Single<Response<PhotoSizesResponsePojo>> getPhotoSizes(String photoId) {
        return photosApiContract.getPhotoSizes(API_KEY, photoId);
    }
}
