package pt.fpapps.flickrapi.photos.model.photo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PublicPhotosResponsePojo {
    @JsonProperty("photos")
    private PublicPhotosPojo publicPhotosPojo;

    public PublicPhotosPojo getPublicPhotosPojo() {
        return publicPhotosPojo;
    }
}
