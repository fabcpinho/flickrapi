package pt.fpapps.flickrapi.photos.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;

import pt.fpapps.flickrapi.R;
import pt.fpapps.flickrapi.dagger.component.DaggerPhotosActivityComponent;
import pt.fpapps.flickrapi.dagger.module.PhotosInteractorModule;
import pt.fpapps.flickrapi.detail.view.DetailActivity;
import pt.fpapps.flickrapi.photos.adapter.PhotosListAdapter;
import pt.fpapps.flickrapi.photos.interactor.PhotosInteractor;
import pt.fpapps.flickrapi.photos.paging.NetworkState;
import pt.fpapps.flickrapi.photos.presenter.PhotosViewModel;
import pt.fpapps.flickrapi.photos.view.contract.PhotosListViewContract;
import pt.fpapps.flickrapi.rxjava.RxSchedulers;

import static pt.fpapps.flickrapi.detail.view.DetailActivity.PHOTO_ID_KEY;
import static pt.fpapps.flickrapi.detail.view.DetailActivity.PHOTO_URL;

public class PhotosActivity extends AppCompatActivity implements PhotosListViewContract {

    /**
     * SOME IMPLEMENTATION NOTES
     *
     * I decided to implement the paginated list with paging library because it's supposely better but
     * mostly because I wanted to try it;
     *
     * Project follows google MVVM architecture with introduction of interactors;
     *
     * Clicking on an item it loads details: only chose title and description to show as proof of concept,
     * and are displayed on the same textview just for speed of implementation
     *
     * Initially I followed an approach of getting all the image URL by the time I get the photo array, but
     * for optimization I took out that responsability and moved it to the adapter. It now requests the Size
     * and downloads the image only by the time the item needs to be drawn;
     *
     * Project handles errors for minimal case scenarios for speed of implementation
     *
     * Possible improvements:
     * - Handle network connectivity
     * - Better user feedback
     * - Refresh button on main activity for scenarios where 1st request fails due to lack of connectivity for instance
     *
     * Stuff currently missing to complete the test:
     * - UI and unit tests
     *
     */



    @Inject
    PhotosInteractor photosInteractor;
    @Inject
    RxSchedulers rxSchedulers;

    private PhotosViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDagger();

        RecyclerView photosRv = findViewById(R.id.photosRecycler);
        ContentLoadingProgressBar progressBar = findViewById(R.id.progressBar);

        photosRv.setLayoutManager(new GridLayoutManager(this, 2));

        PhotosListAdapter photosListAdapter = new PhotosListAdapter(this);
        photosRv.setAdapter(photosListAdapter);

        mViewModel = ViewModelProviders.of(this).get(PhotosViewModel.class);
        mViewModel.init(photosInteractor, rxSchedulers);

        mViewModel.getPhotosLiveData().observe(this, photoPojos -> {
            if (photoPojos.size() != 0) {
                photosListAdapter.submitList(photoPojos);
                progressBar.setVisibility(View.GONE);
            }
        });

        mViewModel.getSizePojoLiveData().observe(this, sizePojo -> photosListAdapter.updateItem(sizePojo.getRvPosition(), sizePojo.getSource()));

        mViewModel.getNetworkState().observe(this, networkState -> {
            if (networkState.getStatus() == NetworkState.Status.FAILED) {
                Toast.makeText(PhotosActivity.this, getResources().getString(R.string.net_problem), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initDagger() {
        DaggerPhotosActivityComponent.builder()
                .photosInteractorModule(new PhotosInteractorModule())
                .build()
                .inject(this);
    }

    @Override
    public void goToDetail(String id, String photoUrl) {
        Bundle bundle = new Bundle();
        bundle.putString(PHOTO_ID_KEY, id);
        bundle.putString(PHOTO_URL, photoUrl);

        Intent i = new Intent(this, DetailActivity.class);
        i.putExtras(bundle);
        startActivity(i);
    }

    @Override
    public void getImageUrl(int position, String id) {
        mViewModel.getPhotoUrl(position, id);
    }
}
