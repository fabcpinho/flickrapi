package pt.fpapps.flickrapi.photos.model.size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SizePojo {
    private int rvPosition;
    private String label;
    private String source;
    private String url;

    public int getRvPosition() {
        return rvPosition;
    }

    public void setRvPosition(int rvPosition) {
        this.rvPosition = rvPosition;
    }

    public String getLabel() {
        return label;
    }

    public String getSource() {
        return source;
    }

    public String getUrl() {
        return url;
    }

}
