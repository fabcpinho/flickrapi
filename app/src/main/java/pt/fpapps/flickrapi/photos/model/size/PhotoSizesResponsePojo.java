package pt.fpapps.flickrapi.photos.model.size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoSizesResponsePojo {
    @JsonProperty("sizes")
    private PhotoSizesPojo photoSizesPojo;

    public PhotoSizesPojo getPhotoSizesPojo() {
        return photoSizesPojo;
    }
}
