package pt.fpapps.flickrapi.photos.view.contract;

public interface PhotosListViewContract {
    void goToDetail(String id, String photoUrl);

    void getImageUrl(int position, String id);
}
