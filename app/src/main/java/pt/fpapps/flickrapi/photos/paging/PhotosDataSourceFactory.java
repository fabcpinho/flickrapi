package pt.fpapps.flickrapi.photos.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import io.reactivex.disposables.CompositeDisposable;
import pt.fpapps.flickrapi.photos.interactor.PhotosInteractor;
import pt.fpapps.flickrapi.photos.model.photo.PhotoPojo;

public class PhotosDataSourceFactory  extends DataSource.Factory<Integer, PhotoPojo> {
    private PhotosDataSource photosDataSource;
    private PhotosInteractor photosInteractor;
    private CompositeDisposable compositeDisposable;
    private MutableLiveData<PhotosDataSource> mutableLiveData;

    public PhotosDataSourceFactory(PhotosInteractor photosInteractor, CompositeDisposable compositeDisposable) {
        this.photosInteractor = photosInteractor;
        this.compositeDisposable = compositeDisposable;
        this.mutableLiveData = new MutableLiveData<>();
    }

    @Override
    public DataSource<Integer, PhotoPojo> create() {
        if(photosDataSource == null){
            photosDataSource = new PhotosDataSource(photosInteractor, compositeDisposable);
            mutableLiveData.postValue(photosDataSource);
        }
        return photosDataSource;
    }

    public MutableLiveData<PhotosDataSource> getMutableLiveData() {
        return mutableLiveData;
    }
}
