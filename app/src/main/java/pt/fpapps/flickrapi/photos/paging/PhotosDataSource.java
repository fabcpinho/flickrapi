package pt.fpapps.flickrapi.photos.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import io.reactivex.disposables.CompositeDisposable;
import pt.fpapps.flickrapi.photos.interactor.PhotosInteractor;
import pt.fpapps.flickrapi.photos.model.photo.PhotoPojo;

public class PhotosDataSource extends PageKeyedDataSource<Integer, PhotoPojo> {
    private PhotosInteractor photosInteractor;
    private CompositeDisposable compositeDisposable;

    private MutableLiveData networkState;

    PhotosDataSource(PhotosInteractor photosInteractor, CompositeDisposable compositeDisposable) {
        this.photosInteractor = photosInteractor;
        this.compositeDisposable = compositeDisposable;

        networkState = new MutableLiveData();
    }

    public MutableLiveData getNetworkState() {
        return networkState;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, PhotoPojo> callback) {
        compositeDisposable.add(photosInteractor.getPublicPhotos(params.requestedLoadSize, 1)
                .retry(2)
                .subscribe(photoPojos -> {
                    callback.onResult(photoPojos, null, 2);
                    networkState.postValue(NetworkState.LOADED);
                }, throwable -> networkState.postValue(new NetworkState(NetworkState.Status.FAILED, throwable.getMessage()))));
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, PhotoPojo> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, PhotoPojo> callback) {
        final int page = params.key;
        compositeDisposable.add(photosInteractor.getPublicPhotos(params.requestedLoadSize, page)
                .retry(2)
                .subscribe(photoPojos -> {
                    callback.onResult(photoPojos, page + 1);
                    networkState.postValue(NetworkState.LOADED);
                }, throwable -> networkState.postValue(new NetworkState(NetworkState.Status.FAILED, throwable.getMessage()))));
    }
}
