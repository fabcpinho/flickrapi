package pt.fpapps.flickrapi.photos.model.size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoSizesPojo {
    @JsonProperty("size")
    private List<SizePojo> sizes;

    public List<SizePojo> getSizes() {
        return sizes;
    }
}
