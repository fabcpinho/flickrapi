package pt.fpapps.flickrapi.photos.presenter;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.disposables.CompositeDisposable;
import pt.fpapps.flickrapi.photos.interactor.PhotosInteractor;
import pt.fpapps.flickrapi.photos.model.photo.PhotoPojo;
import pt.fpapps.flickrapi.photos.model.size.SizePojo;
import pt.fpapps.flickrapi.photos.paging.NetworkState;
import pt.fpapps.flickrapi.photos.paging.PhotosDataSourceFactory;
import pt.fpapps.flickrapi.rxjava.RxSchedulers;

public class PhotosViewModel extends ViewModel {
    private LiveData<PagedList<PhotoPojo>> photosLiveData;
    private LiveData<NetworkState> networkState;
    private MutableLiveData<SizePojo> sizePojoLiveData;
    private CompositeDisposable compositeDisposable;
    private PhotosInteractor photosInteractor;
    private RxSchedulers rxSchedulers;

    public void init(PhotosInteractor photosInteractor, RxSchedulers rxSchedulers) {
        this.photosInteractor = photosInteractor;
        this.rxSchedulers = rxSchedulers;
        compositeDisposable = new CompositeDisposable();
        PhotosDataSourceFactory photosDataSourceFactory = new PhotosDataSourceFactory(photosInteractor, compositeDisposable);

        networkState = Transformations.switchMap(photosDataSourceFactory.getMutableLiveData(),
                dataSource -> dataSource.getNetworkState());

        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(25)
                .setEnablePlaceholders(true)
                .build();

        ExecutorService executor = Executors.newFixedThreadPool(5);

        photosLiveData = new LivePagedListBuilder(photosDataSourceFactory, config)
                .setFetchExecutor(executor)
                .build();

        sizePojoLiveData = new MutableLiveData<>();
    }

    public void getPhotoUrl(int position, String photoId) {
        compositeDisposable.add(photosInteractor.getPhotoSizes(photoId)
                .compose(rxSchedulers.applySingleSchedulerTransformer())
                .retry(2)
                .subscribe(sizePojo -> {
                    sizePojo.setRvPosition(position);
                    sizePojoLiveData.postValue(sizePojo);
                }, throwable -> Log.d("Fabio", "Doing nothing in case of error")));
    }

    public LiveData<PagedList<PhotoPojo>> getPhotosLiveData() {
        return photosLiveData;
    }

    public LiveData<NetworkState> getNetworkState() {
        return networkState;
    }

    public MutableLiveData<SizePojo> getSizePojoLiveData() {
        return sizePojoLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
